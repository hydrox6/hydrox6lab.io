character = {}
character["0"] = [5, 1,0,3,1, 0,1,1,6, 4,1,1,6, 1,7,3,1]
character["1"] = [3, 0,7,3,1, 1,0,1,7, 0,1,1,1]
character["2"] = [5, 0,7,5,1, 1,0,3,1, 0,1,1,1, 4,1,1,2, 0,6,1,1, 1,5,1,1, 2,4,1,1, 3,3,1,1]
character["3"] = [4, 1,7,2,1, 1,0,2,1, 1,3,2,1, 0,1,1,1, 0,6,1,1, 3,1,1,2, 3,4,1,3]
character["4"] = [4, 0,0,1,6, 0,5,4,1, 2,3,1,5]
character["5"] = [4, 0,0,4,1, 0,1,1,3, 1,3,2,1, 3,4,1,3, 1,7,2,1, 0,6,1,1]
character["6"] = [5, 1,7,3,1, 0,2,1,5, 1,4,1,1, 2,3,2,1, 4,4,1,3, 1,1,1,1, 2,0,2,1, 4,1,1,1]
character["7"] = [4, 0,0,4,1, 0,6,1,2, 1,4,1,2, 2,2,1,2, 3,1,1,1]
character["8"] = [5, 1,0,3,1, 1,3,3,1, 1,7,3,1, 0,1,1,2, 4,1,1,2, 0,4,1,3, 4,4,1,3]
character["9"] = [5, 1,0,3,1, 0,1,1,2, 4,1,1,7, 1,3,1,1, 2,4,2,1]
character["m"] = [7, 1,2,2,1, 4,2,2,1, 0,3,1,5, 3,3,1,5, 6,3,1,5]
character["~"] = [4, 0,4,1,1, 1,3,1,1, 2,4,1,1, 3,3,1,1]
character["."] = [1, 0,7,1,1]
function renderText(context,colour,text,x,y)
{
	context.fillStyle = colour;
	var cX = x;
	for(var c = 0, len = text.length; c < len; c++)
	{
		var iter = 2;
		var char = text[c];
		if(char in character)
		{
			var charData = character[char];
			iter += charData[0]
			for(var i = 1, ilen = charData.length; i < ilen; i += 4)
			{

				context.fillRect(cX+charData[i],y+charData[i+1],charData[i+2],charData[i+3]);
			}
		}
		cX += iter;
	}
}

function gen(canvas,ix,iy,scale,distance,pets,data)
{
	var tsize = 32+distance
	var width = (tsize)*ix + distance + 20;
	var height = (tsize)*iy + distance + 28;
	var scaledwidth = width * scale;
	var scaledheight = height * scale;
	canvas.width = scaledwidth;
	canvas.height = scaledheight;

	var context = canvas.getContext("2d");
	context.imageSmoothingEnabled = false
	context.scale(scale,scale);
	context.fillStyle = "#463C32";
	context.fillRect(0,0,width,height);

	//Draw Frame
	var frameStore = document.getElementById("frameStore");

	var frame_l = document.getElementById("frame_l")
	var frame_r = document.getElementById("frame_r")
	var frame_t = document.getElementById("frame_t")
	var frame_b = document.getElementById("frame_b")
    var watermark = document.getElementById("wtmrk")

	for(var y = 0; y < height; y += 48)
	{
		context.drawImage(frame_l,0,y);
		context.drawImage(frame_r,width-7,y);
	}

	for(var x = 0; x < width; x += 48)
	{
		context.drawImage(frame_t,x,0);
		context.drawImage(frame_b,x,height-7);
	}

	var frame_tl = document.getElementById("frame_tl")
	var frame_tr = document.getElementById("frame_tr")
	var frame_bl = document.getElementById("frame_bl")
	var frame_br = document.getElementById("frame_br")

	context.drawImage(frame_tl,0,0)
	context.drawImage(frame_tr,width-12,0)
	context.drawImage(frame_bl,0,height-12)
	context.drawImage(frame_br,width-12,height-12)


	//Draw Pets
	var cX = 10 + distance;
	var cY = 10 + distance;

	context.font = "16px Runescape"

	for(var p = 0; p < pets.length; p++)
	{
		var petimg = document.getElementById(pets[p]);
		if(data[p] == "-1")
		{
			context.globalAlpha = 0.35;
		}
		else
		{
			context.globalAlpha = 1;
		}
		context.drawImage(petimg,cX,cY);

		if(data[p] != "-1")
		{
			renderText(context,"#000000",data[p],cX+1,cY+25);
			renderText(context,"#FFFF00",data[p],cX,cY+24);
		}

		cX += tsize
		if(cX > width-(tsize+10))
		{
			cY += tsize
			cX = 10 + distance
		}
	}
    context.globalAlpha = 0.6;
    context.drawImage(watermark, width-197, height-23)
}